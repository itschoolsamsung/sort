import java.util.*;

public class Test {
    public static void main(String[] args) {
        Integer[] arr = new Integer[]{5, 2, 7, 8, 3};
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();

        ArrayList<Integer> list = new ArrayList<>(Arrays.asList(arr));
        System.out.println(list);

        Arrays.sort(arr);
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();

        Collections.sort(list);
        System.out.println(list);

        class C1 implements Comparator<Integer> {
            @Override
            public int compare(Integer o1, Integer o2) {
                if (o1 > o2) {
                    return -1;
                }
                if (o1 < o2) {
                    return 1;
                }
                return 0;
            }
        }

        C1 c1 = new C1();

        Arrays.sort(arr, c1);
        for (int i : arr) {
            System.out.print(i + " ");
        }
        System.out.println();

        Collections.sort(list, c1);
        System.out.println(list);

        /*
         * Rect
         */

        ArrayList<Rect> rects = new ArrayList<>();
        rects.add( new Rect(1, 1) );
        rects.add( new Rect(4, 3) );
        rects.add( new Rect(2, 5) );

        Collections.sort(rects, new Comparator<Rect>(){
            @Override
            public int compare(Rect o1, Rect o2) {
//                if (o1.area() > o2.area()) {
//                    return 1;
//                }
//                if (o1.area() < o2.area()) {
//                    return -1;
//                }
//                return 0;
                return Integer.compare(o1.area(), o2.area());
            }
        });

        for (Rect r : rects) {
            System.out.println(r.area());
        }

        rects.forEach((v) -> { //Java 8+
            System.out.println(v.area());
        });
    }
}
